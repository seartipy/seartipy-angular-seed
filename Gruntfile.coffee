module.exports = (grunt) ->
  (require 'load-grunt-tasks')(grunt)
  grunt.registerTask 'serve', ['connect:server']
  grunt.registerTask 'build', ['bower', 'concurrent:minify', 'concurrent:sources']
  grunt.registerTask 'default', ['build', 'concurrent:default']
  grunt.registerTask 'minimal', ['concurrent:default']
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    coffee:
      options:
        sourceMap: true
        join: true
      combined:
        files:
          'build/js/<%= pkg.name %>.js': ['src/coffee/**/*.coffee']
    jade:
      compile:
        expand: true
        cwd: 'src/jade'
        src: ['**/*.jade']
        dest: 'build/html/'
        ext: '.html'

    bower:
      install:
        options:
          targetDir: 'build/lib'

    cssmin:
      combine:
        files:
          'build/css/lib.min.css': ['build/lib/**/*.css']

    uglify:
      libs:
        files:
          'build/js/lib.min.js': [
            'build/lib/jquery/jquery.js'
            'src/lib/*.js'
            'build/lib/**/!(jquery).js'
          ]

    connect:
      server:
        options:
          port: 8000
          keepalive: true
          base: ['build/html', 'build/js', 'build/css', 'src/html', 'src/js', 'src/css']

    watch:
      coffee:
        files: ['src/coffee/**/*.coffee']
        tasks: ['coffee']
      jade:
        files: ['src/jade/**/*.jade']
        tasks: ['jade']
      html_css_js:
        files: ['src/js/**/*.js', 'src/html/**/*.html', 'src/css/**/*.css']
        tasks: []
      options:
        spawn: true
        livereload: true

    concurrent:
      minify: ['cssmin', 'uglify']
      sources: ['jade', 'coffee']
      default: ['serve', 'watch']
      options:
        logConcurrentOutput: true
